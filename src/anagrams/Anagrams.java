package anagrams;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

/**
 *  Written SLewis
 *  Usage Anagrams <text file1> <text File2>
 *      I suggest getting test books from
 *
 *   I wanted to write the Java 7 version of Joe's project in Java 8
 *   Neither project has comments
 *   The Java 8 version is MUCH shorter
 *   However
 *       a) Is it more readable?
 *       b) is it maintainable - Test here - the project listed below will handle a file with one word per line
 *           but not a text file and will not filter punctuation - try to add those capabilities and try on a real book like
 *            http://www.gutenberg.org/ebooks/2600.txt.utf-8
 * https://bitbucket.org/joebowbeer/anagrams/src/
 */
public class Anagrams {

     public static final Comparator<Anagram> ByNumberWords = new Comparator<Anagram>() {
        @Override public int compare(final Anagram o1, final Anagram o2) {
            int wc1 = o1.getWordCount();
            int wc2 = o2.getWordCount();
            if (wc1 != wc2) {
                return wc1 > wc2 ? -1 : 1;
            }
            return o1.compareTo(o2);  // compareKeys
        }
    };

      public static final Comparator<Anagram> ByLongest = new Comparator<Anagram>() {
        @Override public int compare(final Anagram o1, final Anagram o2) {
            int wc1 = o1.getKey().length();
            int wc2 = o2.getKey().length();
            if (wc1 != wc2) {
                return wc1 > wc2 ? -1 : 1;
            }
            return o1.compareTo(o2);  // compareKeys
        }
    };

    public static void main(String[] args) throws IOException {
        Anagrams anagrams = new Anagrams();
        assert args.length == 1; // path-to-words
        for (int i = 0; i < args.length; i++) {
            File f = new File(args[i]);
            anagrams.addFile(f);
        }
        System.out.println("Longest");
        for (Anagram longest : anagrams.getlongestWord()) {
            System.out.println(longest);
        }
        System.out.println("Most words ");
        for (Anagram most : anagrams.getMostWords()) {
            System.out.println(most);
        }
    }

    private final ConcurrentHashMap<String, Anagram> m_Anagrams = new ConcurrentHashMap<String, Anagram>();

    public Anagrams() {
    }

    public List<Anagram> getMostWords() {
        List<Anagram> allAnagrams = new ArrayList<Anagram>(m_Anagrams.values());
        if (allAnagrams.size() == 0)
            return null;
        Collections.sort(allAnagrams, ByNumberWords);

        List<Anagram> ret = new ArrayList<Anagram>();
        Anagram first = allAnagrams.get(0);
        int firstSize = first.getWordCount();
         for (Anagram test : allAnagrams) {
            if (test.getWordCount() < firstSize)
                return ret;
            ret.add(test);
        }
        return ret;
    }


    public List<Anagram> getlongestWord() {
        List<Anagram> allAnagrams = new ArrayList<Anagram>(m_Anagrams.values());
        if (allAnagrams.size() == 0)
            return null;
        Collections.sort(allAnagrams, ByLongest);
        Anagram first = allAnagrams.get(0);

        int firstSize = first.getKey().length();
        List<Anagram> ret = new ArrayList<Anagram>();
        for (Anagram test : allAnagrams) {
            if (test.getKey().length() < firstSize)
                return ret;
            ret.add(test);
        }
        return ret;
    }


    public void addFile(File f) {
        try {
            LineNumberReader rdr = new LineNumberReader(new FileReader(f));
            String line = rdr.readLine();
            while (line != null) {
                for (String s : line.split(" ")) {
                    addAnagram(s);
                }
                line = rdr.readLine();
            }
        }
        catch (IOException e) {
            throw new RuntimeException(e);

        }
    }

    public void addAnagram(String word) {
        word = cleanString(word);
        if (word.length() == 0)
            return;
        String key = key(word);
        synchronized (m_Anagrams) {
            Anagram anagram = m_Anagrams.get(key);
            if (anagram == null) {
                new Anagram(word);
            }
            else {
                anagram.addWord(word);
            }
        }
    }


    private class Anagram implements Comparable<Anagram> {
        private final String m_Key;
        private final Set<String> m_Words = new HashSet<String>();

        private Anagram(final String word) {
            m_Key = key(word);
            m_Words.add(word);
            synchronized (m_Anagrams) {
                m_Anagrams.put(m_Key, this);
            }
        }

        private void addWord(final String word) {
            m_Words.add(word);
        }


        public String getKey() {
            return m_Key;
        }

        public String[] getWords() {
            String[] ret = m_Words.toArray(new String[m_Words.size()]);
            Arrays.sort(ret);
            return ret;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(getKey());
            for (String word : getWords()) {
                sb.append(" ");
                sb.append(word);
            }
            return sb.toString();
        }

        public int getWordCount() {
            return m_Words.size();
        }

        @Override public int compareTo(final Anagram o) {
            return getKey().compareTo(o.getKey());
        }
    }


    private static String key(String s) {
        char[] chars = s.toLowerCase().toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }

    private static String cleanString(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (Character.isLetter(c))
                sb.append(Character.toLowerCase(c));
        }
        return sb.toString();
    }


}
